//
//  Flashcard.swift
//  Flashcards
//
//  Created by Grant Davis on 1/25/22.
//

import Foundation

class Flashcard {
    var term: String
    var def: String
    
    init() {
        term = " "
        def = " "
    }
    
    func generateFlashcards()->[Flashcard] {
        var f1 = Flashcard()
        var f2 = Flashcard()
        var f3 = Flashcard()
        var f4 = Flashcard()
        var f5 = Flashcard()
        var f6 = Flashcard()
        var f7 = Flashcard()
        var f8 = Flashcard()
        var f9 = Flashcard()
        var f10 = Flashcard()
        var flashcards = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10]
        return flashcards
    }
}
