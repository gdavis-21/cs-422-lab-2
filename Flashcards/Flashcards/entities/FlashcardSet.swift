//
//  FlashcardSet.swift
//  Flashcards
//
//  Created by Grant Davis on 1/25/22.
//

import Foundation

class FlashcardSet {
    var title: String
    
    init() {
        title = "Default"
    }
    
    func generateFlashcardSets() -> [FlashcardSet] {
        var f1 = FlashcardSet()
        var f2 = FlashcardSet()
        var f3 = FlashcardSet()
        var f4 = FlashcardSet()
        var f5 = FlashcardSet()
        var f6 = FlashcardSet()
        var f7 = FlashcardSet()
        var f8 = FlashcardSet()
        var f9 = FlashcardSet()
        var flashcardSets = [f1, f2, f3, f4, f5, f6, f7, f8, f9]
        return flashcardSets
    }
}
