package cs.mad.flashcards.entities

data class FlashcardSet(var title: String) {

    fun generateFlashcardSets(): Array<FlashcardSet> {
        var f1 = FlashcardSet("1")
        var f2 = FlashcardSet("2")
        var f3 = FlashcardSet("3")
        var f4 = FlashcardSet("4")
        var f5 = FlashcardSet("5")
        var f6 = FlashcardSet("6")
        var f7 = FlashcardSet("7")
        var f8 = FlashcardSet("8")
        var f9 = FlashcardSet("9")
        var f10 = FlashcardSet("10")
        var flashcardSets = arrayOf(f1, f2, f3, f4, f5, f6, f7, f8, f9 ,f10)
        return flashcardSets
    }
}
