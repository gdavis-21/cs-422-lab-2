package cs.mad.flashcards.entities

data class Flashcard(var definition: String, var term: String) {
    init {
        var d = definition
        var t = term
    }

    fun generateFlashcards(): Array<Flashcard> {
        var f1 = Flashcard("1", "1")
        var f2 = Flashcard("2", "2")
        var f3 = Flashcard("3", "3")
        var f4 = Flashcard("4","4")
        var f5 = Flashcard("5", "5")
        var f6 = Flashcard("6", "6")
        var f7 = Flashcard("7", "7")
        var f8 = Flashcard("8","8")
        var f9 = Flashcard("9","9")
        var f10 = Flashcard("10","10")
        var flashcards = arrayOf(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10)
        return flashcards
    }
}





